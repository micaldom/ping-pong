-- chikun :: 2015
-- Configuration file

require("lua/flags")  -- File containing custom flags for game

function love.conf(game)

	game.console = true     -- Attach a console [DISABLE ON DISTRIBUTION]
	game.version = "0.10.0"  -- Version of LÖVE which this game was made for

	-- Omit modules due to disuse
	game.modules.math   = false
	game.modules.thread = false

	-- Various window settings
	game.window.title          = "chikun Game"
	game.window.fullscreen     = FULLSCREEN
	game.window.fullscreentype = "desktop"
	game.window.width          = GAME_WIDTH * DEFAULT_SCALE
	game.window.height         = GAME_HEIGHT * DEFAULT_SCALE
	game.window.minwidth       = GAME_WIDTH
	game.window.minheight      = GAME_HEIGHT
	game.window.resizable      = true
	game.window.vsync          = GAME_VSYNC
end
