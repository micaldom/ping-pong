-- chikun :: 2014-2015
-- Loads all sounds from the /bgm folder


-- Recursively checks a folder for sounds and adds any found to a table
local function loadBGMFolder(dir, tab)

	local tab = (tab or {})

	-- Get a table of files / subdirs from dir
	local items = love.filesystem.getDirectoryItems(dir)

	for key, val in ipairs(items) do

		if (love.filesystem.isFile(dir .. "/" .. val)) then

			if (val ~= "init.lua") then

				-- Remove ".ogg" extension on file
				name = val:sub(1, -5)

				-- Load sound into table
				tab[name] = love.audio.newSource(dir .. "/" .. val, 'stream')
				tab[name]:setLooping(true)
			end
		else

			-- Add new table onto tab
			tab[val] = { }

			-- Run checkFolder in this subdir
			loadSFXFolder(dir .. "/" .. val, tab[val])
		end
	end

	return tab
end


-- Load the bgm folder
return loadBGMFolder("bgm")
