-- chikun :: 2015
-- Intercepts regular love functions with calls to our input library
-- NOTE: Relies on this input library being loaded as ci


function love.gamepadaxis(joystick, axis, value)

	ci:setScheme("xbox")

	ci.inputs["pad_axis_" .. axis .. "_neg"] = math.max(0, -value)
	ci.inputs["pad_axis_" .. axis .. "_pos"] = math.max(0, value)
end


function love.gamepadpressed(joystick, button)

	ci:setScheme("xbox")

	ci.inputs["pad_button_" .. button] = 1
end


function love.gamepadreleased(joystick, button)

	ci:setScheme("xbox")

	ci.inputs["pad_button_" .. button] = 0
end


function love.joystickaxis(joystick, axis, value)

	ci:setScheme("ouya")

	ci.inputs["joy_axis_" .. axis .. "_neg"] = math.max(0, -value)
	ci.inputs["joy_axis_" .. axis .. "_pos"] = math.max(0, value)
end


function love.joystickpressed(joystick, button)

	ci:setScheme("ouya")

	ci.inputs["joy_button_" .. button] = 1
end


function love.joystickreleased(joystick, button)

	ci:setScheme("ouya")

	ci.inputs["joy_button_" .. button] = 0
end


function love.keypressed(key)

	ci:setScheme("keys")

	ci.inputs["key_" .. key] = 1
end


function love.keyreleased(key)

	ci:setScheme("keys")

	ci.inputs["key_" .. key] = 0
end
