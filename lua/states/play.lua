-- chikun :: 2016
-- Play state


-- Temporary state, removed at end of script
local PlayState = class(PrototypeClass, function(new_class) end)

-- Create local variable to hold the players
local players = { }

-- On state create
function PlayState:create()

	-- Create player 1
	local player = {
		x = 30,
		y = GAME_HEIGHT / 2 - 40,
		w = 15,
		h = 80
	}

	-- Create player 2
	local opponent = {
		x = GAME_WIDTH - 45,
		y = GAME_HEIGHT / 2 - 40,
		w = 15,
		h = 80
	}

	-- Insert both players into objects table
	table.insert(players, player)
	table.insert(players, opponent)

	-- Create a score board
	score = {
		0,
		0
	}

	-- Create a ball
	ball = {
		x = GAME_WIDTH / 2 - 5,
		y = GAME_HEIGHT / 2 - 5,
		r = 10,
		angle = math.random(360),
		speed = 1
	}

	while (ball.angle == 90 or ball.angle == 270) do

		ball.angle = math.random(360)
	end
end


-- On state update
function PlayState:update(dt)

	-- PLAYER 1 START
	-- Local variable to move the player
	local playewr_1_y_move = 0

	-- Check if the up button is held
	if ci:isDown("up") then

		-- Make y movement negativel
		playewr_1_y_move = -1
	end

	-- Check if the down button is being held
	if ci:isDown("down") then

		-- Increase y move by one to either 0 or 1
		playewr_1_y_move = playewr_1_y_move + 1
	end

	-- Move the player
	players[1].y = players[1].y + playewr_1_y_move * dt * 120

	-- If player has moved off the top of the screen
	if players[1].y <= 0 then

		-- Player is moved back onto the screen
		players[1].y = 0

	-- If player has moved off the bottom of the screen
	elseif players[1].y >= GAME_HEIGHT - players[1].h then

		-- Player is moved back onto the screen
		players[1].y = GAME_HEIGHT - players[1].h
	end
	-- PLAYER 1 END

	-- BALL START
	-- Local variables to hold the movement of the ball
	local ball_x_move, ball_y_move = 0, 0

	-- Calculate the movement of the ball
	-- In x
	ball_x_move = math.cos(ball.angle / 180 * math.pi)
	-- And y
	ball_y_move = math.sin(ball.angle / 180 * math.pi)

	-- Move ball based on the balls movement
	-- In x
	ball.x = ball.x + ball_x_move * dt * 120 * ball.speed
	-- And Y
	ball.y = ball.y + ball_y_move * dt * 120 * ball.speed

	-- Check if ball is off the top of the screen
	if ball.y <= 0 + ball.r then

		-- Calculate angle of incidence
		local incidence = 270 - ball.angle
		-- Calculate angle of outcidence
		local outcidence = 90 + incidence

		-- Change ball angle to that of the outcidence
		ball.angle = outcidence
		-- Move ball back into play
		ball.y = 0 + ball.r

	-- Check if ball is off bottom of the screen
	elseif ball.y >= GAME_HEIGHT - ball.r then

		-- calculate angle of incidence
		local incidence = 90 - ball.angle
		-- calculate angle of outcidence
		local outcidence = 270 + incidence

		-- Set balls angle to new angle
		ball.angle = outcidence

		-- Move ball back into play
		ball.y = GAME_HEIGHT - ball.r
	end

	-- Check if ball is able to collide with player
	if (ball.x - ball.r <= players[1].x + players[1].w)
		and (ball.x + ball.r >= players[1].x) then

		-- check if ball is colliding with player
		if (ball.y - ball.r <= players[1].y + players[1].h)
			and (ball.y + ball.r >= players[1].y) then

			-- Calculate where the ball hit on the paddle
			local angle = ball.y - (players[1].y - ball.r)

			-- if ball hit top half of the paddle
			if angle <= 45 then

				-- set ball bouncing off on a different angle
				ball.angle = 270 + angle * 2

			-- If ball hit bottom half of the paddle
			else

				-- set ball bouncing off on a different angle
				ball.angle = (angle - 45) * 2
			end

			ball.speed = ball.speed + 0.075
		end
	end

	-- Check if ball is able to collide with player
	if (ball.x - ball.r <= players[2].x + players[2].w)
		and (ball.x + ball.r >= players[2].x) then

		-- check if ball is colliding with player
		if (ball.y - ball.r <= players[2].y + players[2].h)
			and (ball.y + ball.r >= players[2].y) then

			-- Calculate where the ball hit on the paddle
			local angle = ball.y- (players[2].y - ball.r)

			ball.angle = 270 - angle * 2

			ball.speed = ball.speed + 0.075
		end
	end

	-- If ball leaves play
	if (ball.x + ball.r < 0) or (ball.x - ball.r > GAME_WIDTH) then

		-- If ball left play off the right hand side
		if ball.x > GAME_WIDTH then

			-- Increase players score
			score[1] = score[1] + 1

		-- If ball left play off left hand side
		else

			-- Incrase opponents score
			score[2] = score[2] + 1
		end

		-- Reset the ball
		ball = {
			x = GAME_WIDTH / 2 - 5,
			y = GAME_HEIGHT / 2 - 5,
			r = 10,
			angle = math.random(360),
			speed = 1
		}

		while (ball.angle == 90 or ball.angle == 270) do

			ball.angle = math.random(360)
		end
	end
	-- BALL END

	-- PLAYER 2 START
	-- Local variable to control player 2's movement
	local player_2_y_move = 0

	-- If ball is below centre of the paddle
	if (players[2].y + players[2].h / 2 < ball.y) then

		-- Set movement to go down
		player_2_y_move = 1

	-- If ball is above centre of the paddle
	elseif (players[2].y + players[2].h / 2 > ball.y) then

		-- Set movement to go up
		player_2_y_move = -1
	end

	-- Move the opponent
	players[2].y = players[2].y + player_2_y_move * dt * 120

	-- If the paddle has moved onto the other side of the ball
	if ((players[2].y + players[2].h / 2 < ball.y)
			and (player_2_y_move < 0)) or
		((players[2].y + players[2].h / 2 > ball.y)
		and (player_2_y_move > 0)) then

		-- Move paddle to be in line with the ball
		players[2].y = ball.y - players[2].h / 2
	end

	-- Check if player 2 is off top of the screen
	if players[2].y <= 0 then

		-- Move player back into the screen
		players[2].y = 0

		-- Check if player 2 is off the bottom of the screen
	elseif players[2].y + players[2].h >= GAME_HEIGHT then

		-- Move player back onto the screen
		players[2].y = GAME_HEIGHT - players[2].h
	end
	-- PLAYER 2 END
end


-- On state draw
function PlayState:draw()

	-- Set font to smaller size
	lg.setFont(cf.score)

	-- For loop for both players
	for num = 1, 2 do

		-- Draw each player
		lg.rectangle("fill", players[num].x, players[num].y, players[num].w, players[num].h)

		-- Draw the score for each player
		lg.print(score[num], players[num].x, 10)
	end

	-- Draw the ball
	lg.circle("fill", ball.x, ball.y, ball.r)
end


-- On state kill
function PlayState:kill()

	self.x = nil
end


-- Transfer data to state loading script
return PlayState
